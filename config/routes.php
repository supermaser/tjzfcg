<?php
/**
 * Created by PhpStorm.
 * User: huxiuchang
 * Date: 15-9-17
 * Time: 下午1:38
 */

use NoahBuscher\Macaw\Macaw;

Macaw::$root = '/api';

Macaw::any('auth2/access_token(:any)', 'Auth2Controller@accesstoken');
Macaw::any('product/get_pools(:any)', 'ProductController@getPools');
Macaw::any('product/skus(:any)', 'ProductController@skus');
Macaw::any('product/detail(:any)', 'ProductController@detail');
Macaw::any('product/shelf_states(:any)', 'ProductController@shelfStates');
Macaw::any('product/images(:any)', 'ProductController@images');
Macaw::any('product/ratings(:any)', 'ProductController@ratings');
Macaw::any('product/prices(:any)', 'ProductController@prices');
Macaw::any('product/stocks(:any)', 'ProductController@stocks');
Macaw::any('product/get_fittings(:any)', 'ProductController@getFittings');
Macaw::any('product/get_services(:any)', 'ProductController@getServices');
Macaw::any('area/provinces(:any)', 'AreaController@provinces');
Macaw::any('area/cities(:any)', 'AreaController@cities');
Macaw::any('area/getCounty(:any)', 'AreaController@getCounty');
Macaw::any('order/submit(:any)', 'OrderController@submit');
Macaw::any('order/confirm(:any)', 'OrderController@confirm');
Macaw::any('order/cancel(:any)', 'OrderController@cancel');
Macaw::any('order/select(:any)', 'OrderController@select');
Macaw::any('order/return(:any)', 'OrderController@returnitem');
Macaw::any('order/track(:any)', 'OrderController@track');
Macaw::any('order/checkr(:any)', 'OrderController@checkr');
Macaw::any('remit/confirm(:any)', 'RemitController@confirm');
Macaw::any('remit/select(:any)', 'RemitController@select');
Macaw::any('invoice/getInvoiceList(:any)', 'InvoiceController@getInvoiceList');

Macaw::error(function() {
  throw new Exception("路由无匹配项 404 Not Found");
});

Macaw::dispatch();