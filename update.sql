#新增接口缓存时间字段
alter table shop2_users add apiTimeout int(11) default 43200;

#新增型号字段
alter table shop2_zmaxshop_item add typeno varchar(255) default NULL;

#新增重量字段
alter table shop2_zmaxshop_item add weight varchar(255) default NULL;

#新增上下架状态字段
alter table shop2_zmaxshop_item add zcstate tinyint(1) default 0;

#新增品牌id字段
alter table shop2_zmaxshop_item add brand_id int(11);

#新建品牌表
DROP TABLE IF EXISTS `shop2_zmaxshop_brand`;
CREATE TABLE `shop2_zmaxshop_brand` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#新增产地
alter table shop2_zmaxshop_item add product_area varchar(255) default NULL;

#新增条形码
alter table shop2_zmaxshop_item add upc varchar(255) default NULL;

#新增销售单位
alter table shop2_zmaxshop_item add unit varchar(255) default NULL;

#新增商品评分
alter table shop2_zmaxshop_item add pj_average varchar(50) default 0;

#新增好评度
alter table shop2_zmaxshop_item add pj_good varchar(50) default 0;

#新增中评度
alter table shop2_zmaxshop_item add pj_medium varchar(50) default 0;

#新增差评度
alter table shop2_zmaxshop_item add pj_bad varchar(50) default 0;

#新增协议优惠价
alter table shop2_zmaxshop_item add zcprice varchar(50);

#新增地址字符串
alter table shop2_zmaxshop_item add area varchar(255);

#订单表新增采购单位名称
alter table shop2_zmaxshop_order add dep_name varchar(255) DEFAULT NULL;
#订单表新增支付方式1:货到付款,2:国库集中支付(转帐), 3:在线支付, 4:支票
alter table shop2_zmaxshop_order add payment varchar(255) DEFAULT NULL;
#订单表新增运费
alter table shop2_zmaxshop_order add freight varchar(255) DEFAULT NULL;
#订单表新增订单模式:1-协议价(默讣);2-团购模式; 3-特惠模式; 4-阶梯价模式
alter table shop2_zmaxshop_order add mode varchar(255) DEFAULT NULL;
#订单表新增商城订单号
alter table shop2_zmaxshop_order add yggc_order varchar(255) DEFAULT NULL;

#订单表新增j状态
alter table shop2_zmaxshop_order add zcstate tinyint(1) default 1

#地址表新增开票人
alter table shop2_zmaxshop_address add drawer_name varchar(255) DEFAULT NULL;
#地址表新增邮编
alter table shop2_zmaxshop_address add zip varchar(255) DEFAULT NULL;
#地址表新增手机号
alter table shop2_zmaxshop_address add mobile varchar(255) DEFAULT NULL;
#地址表新增手email
alter table shop2_zmaxshop_address add email varchar(255) DEFAULT NULL;
#地址表新增增值票收票人
alter table shop2_zmaxshop_address add invoice_name varchar(255) DEFAULT NULL;
#地址表新增注册电话
alter table shop2_zmaxshop_address add invoice_phone varchar(255) DEFAULT NULL;
#地址表新增开户银行
alter table shop2_zmaxshop_address add invoice_bank varchar(255) DEFAULT NULL;


#订单表新增商城订单号
alter table shop2_zmaxshop_order add shipment_id varchar(255) DEFAULT NULL;

#订单表新增开票状态
alter table shop2_zmaxshop_order add invoice_state  tinyint(1) default 0;

CREATE TABLE `shop2_zmaxshop_orderremit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `remit_code` varchar(255) DEFAULT NULL COMMENT '汇款 code',
  `remit_company_name` varchar(255) DEFAULT NULL COMMENT '汇款单位名称',
  `remit_company_bank` varchar(255) DEFAULT NULL COMMENT '汇款单位银行',
  `remit_company_account` varchar(255) DEFAULT NULL COMMENT '汇款单位账号',
  `remit_price` varchar(255) DEFAULT NULL COMMENT '汇款金额',
  `remit_at` varchar(255) DEFAULT NULL COMMENT '汇款时间(yyyy-MM-dd)',
  `receive_company_name` varchar(255) DEFAULT NULL COMMENT '收款单位名称',
  `receive_company_bank` varchar(255) DEFAULT NULL COMMENT '收款单位银行',
  `receive_company_account` varchar(255) DEFAULT NULL COMMENT '收款单位账号',
  `member_account` varchar(255) DEFAULT NULL COMMENT '会员账号',
  `client_num` varchar(255) DEFAULT NULL COMMENT '客户编码',
  `sale_org` varchar(255) DEFAULT NULL COMMENT '订单对应销售公司',
  `order_ids` varchar(255) DEFAULT NULL COMMENT '订单号',
  `deal_result` tinyint(1) DEFAULT '5',
  `create_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;


#新增延保
alter table shop2_zmaxshop_item add service_ids varchar(255) default NULL;
alter table shop2_zmaxshop_item add service_name varchar(255) default NULL;
alter table shop2_zmaxshop_item add service_url varchar(255) default NULL;

CREATE TABLE `shop2_zmaxshop_itemservice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sku` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `create_at` datetime DEFAULT '0000-00-00 00:00:00',
  `update_at` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;