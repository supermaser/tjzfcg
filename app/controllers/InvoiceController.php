<?php
class InvoiceController extends BaseController
{
    /*电子发票接口*/
    public function getInvoiceList()
    {
    	$access_token = $_REQUEST['token'];
    	parent::checkToken($access_token);

        $args = $_REQUEST;

        $order_ids = explode(',', $args['order_ids']);

		$invoices = Invoice::whereIN('order_no', $order_ids)->get(['order_no','einvoice']);
        $data['success'] = true;
        $data['result'] = [];
        foreach ($invoices as $key => $value) {
            $data['result'][$value['order_no']] = 'http://www.tjzfcg.com/media/zmaxshop/tax/'.$value['einvoice'];
        }
		echo json_encode($data, JSON_UNESCAPED_UNICODE);
		exit;
    }
}