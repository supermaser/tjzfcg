<?php
class Auth2Controller extends BaseController
{
	/*授权获取 Access Token*/
    public function accesstoken()
    {
    	//验证参数
    	$username = $_REQUEST['username'];
    	$aes_password = $_REQUEST['password'];
    	$timestamp = $_REQUEST['timestamp'];

    	$sign = $_REQUEST['sign'];
        // echo parent::aesEncode($aes_password, $username, true);
        // echo md5($username . $aes_password . $timestamp . $aes_password);
        // exit;
    	//验证签名
    	if($sign != md5($username . $aes_password . $timestamp . $aes_password)){
    		$result['success'] = false;
	    	$result['desc'] = "illegal_signature";
    	} else {
    		//查询用户账户密码是否正确
    		$user = User::where('username', '=', $username)->first();

    		if ($user) {
    			$match = parent::verifyPassword($username, $aes_password, $user->password, $user->id);
	    		if ($match === true)
				{
					//生成token
		    		$set_token = parent::setToken($user);
			    	$result['success'] = true;
			    	$result['access_token'] = $set_token['access_token'];
			    	$result['expires_at'] = date('Y-m-d H:i:s', $set_token['expires_at']);
				} else {
					$result['success'] = false;
		    		$result['desc'] = "password_error";
				}
    		} else {
    			$result['success'] = false;
		    		$result['desc'] = "the_account_does_not_exist";
    		}
    		
    		
    	}
        echo json_encode($result, JSON_UNESCAPED_UNICODE);	
        exit;
    }
}