<?php
class AreaController extends BaseController
{
    /*获取一级地址*/
    public function provinces()
    {
    	$access_token = $_REQUEST['token'];
    	parent::checkToken($access_token);
		$provinces = Province::get(['province_id', 'province']);
		$result = [];
		foreach ($provinces as $key => $value) {
			$result[$value['province']] = $value['province_id'];
		}
		$data['success'] = true;
		$data['desc'] = '';
		$data['result'] = $result;
		echo json_encode($data, JSON_UNESCAPED_UNICODE);
		exit;
    }

    /*获取二级地址*/
    public function cities()
    {
    	$access_token = $_REQUEST['token'];
    	$province_id = $_REQUEST['id'];
    	parent::checkToken($access_token);
		$provinces = City::where('father', '=', $province_id)->get(['city_id', 'city']);
		$result = [];
		foreach ($provinces as $key => $value) {
			$result[$value['city']] = $value['city_id'];
		}
		$data['success'] = true;
		$data['desc'] = '';
		$data['result'] = $result;
		echo json_encode($data, JSON_UNESCAPED_UNICODE);
		exit;
    }

     /*获取三级地址*/
    public function getCounty()
    {
    	$access_token = $_REQUEST['token'];
    	$province_id = $_REQUEST['id'];
    	parent::checkToken($access_token);
		$provinces = County::where('father', '=', $province_id)->get(['area_id', 'area']);
		$result = [];
		foreach ($provinces as $key => $value) {
			$result[$value['area']] = $value['area_id'];
		}
		$data['success'] = true;
		$data['desc'] = '';
		$data['result'] = $result;
		echo json_encode($data, JSON_UNESCAPED_UNICODE);
		exit;
    }
}