<?php
class OrderController extends BaseController
{
    /*预下订单接口*/
    public function submit()
    {
    	$access_token = $_REQUEST['token'];
    	parent::checkToken($access_token);
        $timestamp = time();
        $args = $_REQUEST;
        $sku = json_decode($args['sku']);
        $order = Order::where('yggc_order', '=', $args['yggc_order'])->first();
        if (isset($order->state)) {
            if($order->state < 1) {
             //更新地址表
            $address_field['name'] = $args['name'];
            $address_field['drawer_name'] = $args['drawer_name'];
            $address_field['province'] = !empty($args['province']) ? $args['province'] : '120000';//$args['province'];
            $address_field['city'] = !empty($args['city']) ? $args['city'] : '120100';//$args['city'];
            $address_field['area'] = !empty($args['county']) ? $args['county'] : '120107';//$args['county'];
            $address_field['street'] = $args['address'];
            $address_field['address'] = $args['address'];
            $address_field['zip'] = $args['zip'];
            $address_field['phone'] = !empty($args['phone']) ? $args['phone'] : '';
            $address_field['mobile'] = !empty($args['mobile']) ? $args['mobile'] : '';
            $address_field['company_name'] = $args['invoice_title'];
            $address_field['tax_tyle'] = $args['invoice_type'];
            $address_field['tax_no'] = isset($args['invoice_org_code']) ? $args['invoice_org_code'] : '';
            $address_field['invoice_name'] = isset($args['invoice_name']) ? $args['invoice_name'] : '';
            $address_field['invoice_phone'] = isset($args['invoice_phone']) ? $args['invoice_phone'] : '';
            $address_field['invoice_bank'] = isset($args['invoice_bank']) ? $args['invoice_bank'] : '';
            $address_field['bank_account'] = isset($args['invoice_bank_code']) ? $args['invoice_bank_code'] : '';
            $address_field['company_address'] = isset($args['invoice_address']) ? $args['invoice_address'] : '';
            $address_id = Address::insert($address_field);
     
            //插入订单表
            $order_field['yggc_order'] = $args['yggc_order'];
            $user = RedisCache::get($access_token);
            $user = unserialize($user);
            $order_field['user_id'] = $user->id;
            $order_field['address_id'] = $address_id;
            $order_field['title'] = '';
            Orderitem::where('order_no', '=', $order->order_no)->delete();
            foreach ($sku as $key => $value) {
                $item = Item::where('number', '=', $value->sku)->first();
                $order_field['title'] .= $item->title;
                $orderitem_field['order_no'] = $order->order_no;
                $orderitem_field['user_id'] = $order_field['user_id'];
                $orderitem_field['product_id'] = $item->id;
                $orderitem_field['price'] = $value->price;
                $orderitem_field['count'] = $value->num;
                $orderitem_field['date'] = date('Y-m-d H:i:s', $timestamp);
                $orderitem_field['attr'] = !empty($value->yanbao) ? json_encode($value->yanbao) : '';
                $orderitem_field['remark'] = '';
                Orderitem::insert($orderitem_field);
            }

            $order_field['description'] = $order_field['title'];
            $order_field['price'] = $args['order_price'];
            $order_field['post_date'] = date('Y-m-d H:i:s', $timestamp);
            $order_field['state'] = 0;
            $order_field['zcstate'] = 0;
            $order_field['pay_type'] = 'offline';
            $order_field['dep_name'] = isset($args['dep_name']) ? $args['dep_name'] : '';
            $order_field['remark'] = $args['remark'];
            $order_field['payment'] = $args['payment'];
            $order_field['freight'] = $args['freight'];
            $order_field['mode'] = $args['mode'];
            Order::where('yggc_order', '=', $args['yggc_order'])->update($order_field);
            $data['success'] = true;
            $data['result']['mall_order_id'] = $order->order_no;
            $data['result']['sku'] = $sku;
            $data['result']['orderPrice'] = $order->price;
            $data['result']['freight'] = $order->freight;
            } else {
                $data['success'] = false;
                $data['desc'] = '商城的订单单号已经存在';
            }
        } else {
            //插入地址表
            $address_field['name'] = $args['name'];
            $address_field['drawer_name'] = $args['drawer_name'];
            $address_field['province'] = !empty($args['province']) ? $args['province'] : '120000';//$args['province'];
            $address_field['city'] = !empty($args['city']) ? $args['city'] : '120100';//$args['city'];
            $address_field['area'] = !empty($args['county']) ? $args['county'] : '120107';//$args['county'];
            $address_field['street'] = $args['address'];
            $address_field['address'] = $args['address'];
            $address_field['zip'] = $args['zip'];
            $address_field['phone'] = $args['phone'];
            $address_field['mobile'] = $args['mobile'];
            $address_field['company_name'] = $args['invoice_title'];
            $address_field['tax_tyle'] = $args['invoice_type'];
            $address_field['tax_no'] = isset($args['invoice_org_code']) ? $args['invoice_org_code'] : '';
            $address_field['invoice_name'] = isset($args['invoice_name']) ? $args['invoice_name'] : '';
            $address_field['invoice_phone'] = isset($args['invoice_phone']) ? $args['invoice_phone'] : '';
            $address_field['invoice_bank'] = isset($args['invoice_bank']) ? $args['invoice_bank'] : '';
            $address_field['bank_account'] = isset($args['invoice_bank_code']) ? $args['invoice_bank_code'] : '';
            $address_field['company_address'] = isset($args['invoice_address']) ? $args['invoice_address'] : '';
            $address_id = Address::insert($address_field);
     
            //插入订单表
            $order_field['order_no'] = parent::newOrderNo();
            $order_field['yggc_order'] = $args['yggc_order'];
            $user = RedisCache::get($access_token);
            $user = unserialize($user);
            $order_field['user_id'] = $user->id;
            $order_field['address_id'] = $address_id;
            $order_field['title'] = '';
            foreach ($sku as $key => $value) {
                $item = Item::where('number', '=', $value->sku)->first();
                $order_field['title'] .= $item->title;
                $orderitem_field['order_no'] = $order_field['order_no'];
                $orderitem_field['user_id'] = $order_field['user_id'];
                $orderitem_field['product_id'] = $item->id;
                $orderitem_field['price'] = $value->price;
                $orderitem_field['count'] = $value->num;
                $orderitem_field['date'] = date('Y-m-d H:i:s', $timestamp);
                $orderitem_field['attr'] = !empty($value->yanbao) ? json_encode($value->yanbao) : '';
                $orderitem_field['remark'] = '';
                Orderitem::insert($orderitem_field);
            }

            $order_field['description'] = $order_field['title'];
            $order_field['price'] = $args['order_price'];
            $order_field['post_date'] = date('Y-m-d H:i:s', $timestamp);
            $order_field['state'] = 0;
            $order_field['zcstate'] = 0;
            $order_field['pay_type'] = 'offline';
            $order_field['dep_name'] = isset($args['dep_name']) ? $args['dep_name'] : '';
            $order_field['remark'] = $args['remark'];
            $order_field['payment'] = $args['payment'];
            $order_field['freight'] = $args['freight'];
            $order_field['mode'] = $args['mode'];
            $address_id = Order::insert($order_field);

            $data['success'] = true;
            $data['result']['mall_order_id'] = $order_field['order_no'];
            $data['result']['sku'] = $sku;
            $data['result']['orderPrice'] = $order_field['price'];
            $data['result']['freight'] = $order_field['freight'];
        }
        
		echo json_encode($data, JSON_UNESCAPED_UNICODE);
		exit;
    }

    /*确认订单接口*/
    public function confirm()
    {
        $access_token = $_REQUEST['token'];
        parent::checkToken($access_token);

        $args = $_REQUEST;
        $timestamp =time();
        $order_no = $args['order_id'];
        $order_field['zcstate'] = 2;
        $order_field['modify_date'] = date("Y-m-d H:i:s", $timestamp);
        if (Order::where('order_no', '=', $order_no)->update($order_field)) {
            $data['success'] = true;
        } else {
            $data['success'] = false;
        }
        
        echo json_encode($data, JSON_UNESCAPED_UNICODE);
        exit;
    }

     /*取消订单接口*/
    public function cancel()
    {
        $access_token = $_REQUEST['token'];
        parent::checkToken($access_token);

        $args = $_REQUEST;
        $timestamp =time();
        $order_no = $args['order_id'];
        $order_field['zcstate'] = 5;
        $order_field['modify_date'] = date("Y-m-d H:i:s", $timestamp);
        if (Order::where('order_no', '=', $order_no)->update($order_field)) {
            $data['success'] = true;
        } else {
            $data['success'] = false;
        }
        echo json_encode($data, JSON_UNESCAPED_UNICODE);
        exit;
    }

     /*查询贵方订单信息接口*/
    public function select()
    {
        $access_token = $_REQUEST['token'];
        parent::checkToken($access_token);

        $args = $_REQUEST;
        $timestamp =time();
        $order_no = $args['order_id'];
        $data['result'] = Order::where('order_no', '=', $order_no)->first(['order_no AS order_id','zcstate AS state', 'price']);

        $orderitem = Orderitem::where('order_no', '=', $order_no)->get();
        $return_skus = $skus = [];
        $data['result']['total_price'] = $data['result']['price'];
        unset($data['result']['price']);
        foreach ($orderitem as $key => $value) {
            $skus[$key]['sku'] = Item::find($value->product_id, ['number'])->number;
            $skus[$key]['num'] = $value->count;
            $skus[$key]['price'] = $value->price;
            if(!empty($value->attr)) {
                $skus[$key]['yanbao'] = json_decode($value->attr);
            }
            if(in_array($value->type, [4,5])) {
                $return_skus[$key]['sku'] = Item::find($value->product_id, ['number'])->number;
                $return_skus[$key]['num'] = $value->count;
                $return_skus[$key]['price'] = $value->price;
                if(!empty($value->attr)) {
                    $return_skus[$key]['yanbao'] = json_decode($value->attr);
                }
            }
           
        }
        $data['result']['skus'] = $skus;
        $data['result']['return_skus'] = $return_skus;
        $data['success'] = true;
        
        echo json_encode($data, JSON_UNESCAPED_UNICODE);
        exit;
    }

    /*退换货接口*/
    public function returnitem()
    {
        $access_token = $_REQUEST['token'];
        parent::checkToken($access_token);

        $args = $_REQUEST;
        $timestamp =time();
        $order_no = $args['order_id'];
        $skus = $args['skus'];
        $type = $args['type'];

        $skus = json_decode($args['skus']);
        $order_field['title'] = '';
        foreach ($skus as $key => $value) {
            $item = Item::where('number', '=', $value->sku)->first();
            $order_field['title'] .= $item->title;
            $orderitem_field['type'] = $type;
            Orderitem::where('product_id', '=', $item->id)->where('order_no', '=', $order_no)->update($orderitem_field);
        }
        $data['success'] = true;
        $data['result']['order_id'] =  $order_no;
        $data['result']['skus'] = $skus;
        $data['result']['type'] = $type;
        echo json_encode($data, JSON_UNESCAPED_UNICODE);
        exit;
    }

    /*物流接口*/
    public function track()
    {
        $access_token = $_REQUEST['token'];
        parent::checkToken($access_token);

        $args = $_REQUEST;

        $order_no = $args['order_id'];
        $order = Order::where('order_no', '=', $order_no)->first();
        if(!empty($order->express_id)) {
            $express = Express::where('id', '=', $order->express_id)->first();
            $data['desc'] = '同一订单批量获取物流信息成功';
            $data['success'] = true;
            $data['result'][0]['order_id'] = $order_no;
            $data['result'][0]['logistic_name'] = $express->company;
            $data['result'][0]['logistic_no'] = $order->express_no;
            $data['result'][0]['shipment_id'] = $express->shipment_id;
            $orderitems = Orderitem::where('order_no', '=', $order_no)->get();
            $data['result'][0]['shipment_items'] = [];
            foreach ($orderitems as $key => $value) {
                $item = Item::where('id', '=', $value->product_id)->first();
                $data['result'][0]['shipment_items'][$key]['name'] = $item->title;
                $data['result'][0]['shipment_items'][$key]['num'] = $value->count;
                $data['result'][0]['shipment_items'][$key]['sku'] = $item->number;
            }
            $data['result'][0]['track'] = [];
            echo json_encode($data, JSON_UNESCAPED_UNICODE);
            exit;
        }
    }

    /*对账接口*/
    public function checkr()
    {
        $access_token = $_REQUEST['token'];
        parent::checkToken($access_token);

        $args = $_REQUEST;

        $bdate = (string)$args['bdate'];
        $edate = (string)$args['edate'];
        $data['success'] = true;
        $data['result']['orders'] = Order::where('post_date', '>', $bdate)->where('post_date', '<', $edate)->get(['order_no AS order_id', 'zcstate AS state', 'invoice_state', 'price AS total_price']);
        $data['result']['num'] = Order::where('post_date', '>', $bdate)->where('post_date', '<', $edate)->count();
        echo json_encode($data, JSON_UNESCAPED_UNICODE);
        exit;
    }
}