<?php
class RemitController extends BaseController
{
    /*汇款信息提交接口*/
    public function confirm()
    {
    	$access_token = $_REQUEST['token'];
    	parent::checkToken($access_token);

        $args = $_REQUEST;

        $field['remit_code'] = $args['remit_code'];
        $field['remit_company_name'] = $args['remit_company_name'];
        $field['remit_company_bank'] = $args['remit_company_bank'];
        $field['remit_company_account'] = $args['remit_company_account'];
        $field['remit_price'] = $args['remit_price'];
        $field['remit_at'] = $args['remit_at'];
        $field['receive_company_name'] = $args['receive_company_name'];
        $field['receive_company_bank'] = $args['receive_company_bank'];
        $field['receive_company_account'] = $args['receive_company_account'];
        $field['member_account'] = $args['member_account'];
        $field['client_num'] = $args['client_num'];
        $field['sale_org'] = $args['sale_org'];
        $field['order_ids'] = $args['order_ids'];
        $remit_id = Remit::insert($field);
    
        $data['success'] = true;
        $data['desc'] = '成功！';
        echo json_encode($data, JSON_UNESCAPED_UNICODE);
        exit;
    }

    /*账期信息查询接口*/
    public function select()
    {
        $access_token = $_REQUEST['token'];
        parent::checkToken($access_token);

        $args = $_REQUEST;

        $remit_code = $args['remit_code'];

        $remit = Remit::where('remit_code', '=', $remit_code)->first(['deal_result']);
        if(!isset($remit->deal_result) || $remit->deal_result<5) {
            $data['success'] = false;
            $data['desc'] = '汇款 code查询错误';
            echo json_encode($data, JSON_UNESCAPED_UNICODE);
            exit;
        }
        $deal_result = [
                                    1=>'销单完成',
                                    2=>'汇款单位或收款单位不正确',
                                    3=>'汇款金额不足以支付订单',
                                    4=>'汇款金额超过订单金额',
                                    5=>'未处理'
                                ];

        $data['success'] = true;
        $data['desc'] = $deal_result[$remit->deal_result];
        $data['result']['deal_result'] = $remit->deal_result;
        echo json_encode($data, JSON_UNESCAPED_UNICODE);
        exit;
    }
}