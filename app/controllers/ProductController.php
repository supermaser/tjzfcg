<?php
class ProductController extends BaseController
{
    //获取品目池接口
    public function getPools()
    {
    	$access_token = $_REQUEST['token'];
    	parent::checkToken($access_token);
		$category = Category::where('extension', '=', 'com_zmaxshop')->where('level', '=', 4)->where('published', '=', 1)->get(['alias','title']);
        $result = [];
        foreach ($category as $key => $value) {
            $result[$key]['id'] = $value->alias;
            $result[$key]['name'] = $value->title;
        }
		$data['success'] = true;
		$data['result'] = $result;
		echo json_encode($data, JSON_UNESCAPED_UNICODE);
		exit;
    }

    //获取商品编号 SKU 接口
    public function skus()
    {
    	$access_token = $_REQUEST['token'];
    	$catalog_id = $_REQUEST['catalog_id'];
    	parent::checkToken($access_token);
        $category = Category::where('alias', '=', $catalog_id)->first(['id']);
        if(isset($category->id)) {
            $item = Item::where('catid', '=', $category->id)->where('published', '=', 1)->get(['number'])->toArray();
        }
		$data['success'] = true;
		$data['result'] = $item ? array_column($item, 'number') : [];
		echo json_encode($data, JSON_UNESCAPED_UNICODE);
		exit;
    }

    //商品详情接口
    public function detail()
    {
    	$access_token = $_REQUEST['token'];
    	$sku = $_REQUEST['sku'];
    	parent::checkToken($access_token);
		$item = Item::where('number', '=', $sku)->where('published', '=', 1)->first(['id','number AS sku', 'typeno','weight', 'image_url AS image_path', 'zcstate AS state', 'title AS name', 'product_area', 'upc', 'unit', 'catid AS category', 'server_promise AS service', 'description AS introduction', 'spare3',  'spare5 AS param', 'attr_normal', 'spare6']);
        if ($item) {
            $item->url = 'http://www.tjzfcg.com/index.php?option=com_zmaxshop&view=item&id='.$item->id;
            $item->image_path = 'http://www.tjzfcg.com/'.$item->image_path;
            unset($item->id);
            $item->introduction = parent::replacePicUrl($item->introduction, 'http://www.tjzfcg.com/');
            $item->service =json_decode($item->service);
            $item->category = Category::where('id', '=', $item->category)->first(['alias'])->alias;
            $item->brand_name = Brand::where('id', '=', $item->brand_id)->value('name');
            $spare3 = json_decode($item->spare3);
            $fields = Field::get(['label', 'name']);
            $field = [];
            foreach ($fields as $key => $value) {
                $field[$value['name']] = $value['label'];
            }
            $item->brand_id = !empty($spare3->brand_id) ? $spare3->brand_id : null;
            $item->brand_name = !empty($spare3->brand_name) ? $spare3->brand_name : null;
            foreach ($spare3 as $key => $value) {
                $main_property_arigin[$field[$key]] = $value;
            }
            $item->main_property_arigin = $main_property_arigin;

            $attr_normal = json_decode($item->attr_normal);
            $item->briefconfig = isset($attr_normal->briefconfig) ? $attr_normal->briefconfig : null;
            $item->ware = isset($attr_normal->energy) ? $attr_normal->ware : null;
            $item->sale_actives = isset($attr_normal->sale_actives) ? $attr_normal->sale_actives : null;
            $item->energy = isset($attr_normal->energy) ? $attr_normal->energy : null;
            $item->protection = isset($attr_normal->protection) ? $attr_normal->protection : null;
            $item->deliverDay = isset($attr_normal->deliverDay) ? $attr_normal->deliverDay : null;
            $item->EfficientGoodsBrand = isset($attr_normal->EfficientGoodsBrand) ? $attr_normal->EfficientGoodsBrand : null;
            $item->EfficientGoodsXinhao = isset($attr_normal->EfficientGoodsXinhao) ? $attr_normal->EfficientGoodsXinhao : null;
            $item->EfficientGoodsSn = isset($attr_normal->EfficientGoodsSn) ? $attr_normal->EfficientGoodsSn : null;
            $item->EfficientGoodsExpiry = isset($attr_normal->EfficientGoodsExpiry) ? $attr_normal->EfficientGoodsExpiry : null;
            $item->EnvmFriendlyGoodsBrand = isset($attr_normal->EnvmFriendlyGoodsBrand) ? $attr_normal->EnvmFriendlyGoodsBrand : null;
            $item->EnvmFriendlyGoodsXinhao = isset($attr_normal->EnvmFriendlyGoodsXinhao) ? $attr_normal->EnvmFriendlyGoodsXinhao : null;
            $item->EnvmFriendlyGoodsSn = isset($attr_normal->EnvmFriendlyGoodsSn) ? $attr_normal->EnvmFriendlyGoodsSn : null;
            $item->EnvmFriendlyGoodsExpiry = isset($attr_normal->EnvmFriendlyGoodsExpiry) ? $attr_normal->EnvmFriendlyGoodsExpiry : null;

            $spare6 = json_decode($item->spare6);
            $item->pc_param = [];
            if ($spare6) {
                foreach ($spare6 as $key => $value) {
                    $pc_param[$field[$key]] = $value;
                }
                $item->pc_param = $pc_param;
            }
            
            unset($item->attr_normal);
            unset($item->spare3);
            unset($item->spare6);
            $data['success'] = true;
            $data['result'] = $item;
        } else {
            $data['success'] = false;
            $data['desc'] = '商品不存在';
        }
        
		echo json_encode($data, JSON_UNESCAPED_UNICODE);
		exit;
    }

    //上下架状态接口
    public function shelfStates()
    {
    	$access_token = $_REQUEST['token'];
    	$sku = explode(',', $_REQUEST['sku']);
    	parent::checkToken($access_token);
		$item = Item::whereIn('number', $sku)->get(['number AS sku', 'zcstate AS state']);
		$data['success'] = true;
		$data['result'] = $item;
		echo json_encode($data, JSON_UNESCAPED_UNICODE);
		exit;
    }

    //图片接口
    public function images()
    {
    	$access_token = $_REQUEST['token'];
    	$sku = explode(',', $_REQUEST['sku']);
    	parent::checkToken($access_token);
		$item = Item::whereIn('number', $sku)->orderBy('ordering')->get(['number', 'images']);

        $result =[];
		foreach ($item as $key => $value) {
            $result[$key]['sku'] = $value->number;
            $images = json_decode($value->images);
            $i=0;
            foreach ($images as $k => $v) {
                if (!empty($v)) {
                    $result[$key]['images'][$i]['path'] = 'http://www.tjzfcg.com/'.$v;
                    $result[$key]['images'][$i]['order'] = $i+1;
                $i++;
                }
               
            }
			
		}
		$data['success'] = true;
		$data['result'] = $result;
		echo json_encode($data, JSON_UNESCAPED_UNICODE);
		exit;
    }

    //商品好评度接口
    public function ratings()
    {
    	$access_token = $_REQUEST['token'];
    	$sku = explode(',', $_REQUEST['sku']);
    	parent::checkToken($access_token);
		$item = Item::whereIn('number', $sku)->orderBy('ordering')->get(['number', 'pj_average', 'pj_good', 'pj_medium', 'pj_bad']);
        $result =[];
        foreach ($item as $key => $value) {
            $result[$key]['sku'] = $value->number;
            $result[$key]['average'] = $value->pj_average;
            $result[$key]['good'] = sprintf("%.2f", $value->pj_good);
            $result[$key]['medium'] = sprintf("%.2f", $value->pj_medium);
            $result[$key]['bad'] = sprintf("%.2f", $value->pj_bad);
        }
		$data['success'] = true;
		$data['result'] = $result;
		echo json_encode($data, JSON_UNESCAPED_UNICODE);
		exit;
    }

    //价格接口
    public function prices()
    {
    	$access_token = $_REQUEST['token'];
    	$sku = explode(',', $_REQUEST['sku']);
    	parent::checkToken($access_token);
		$item = Item::whereIn('number', $sku)->orderBy('ordering')->get(['number AS sku', 'price AS mall_price', 'zcprice AS price']);
		$data['success'] = true;
		$data['result'] = $item;
		echo json_encode($data, JSON_UNESCAPED_UNICODE);
		exit;
    }

    /*库存接口*/
    public function stocks()
    {
        $access_token = $_REQUEST['token'];
        parent::checkToken($access_token);
        $args = $_REQUEST;

        $sku = explode(',', $args['sku']);
        $item =[];
        if (is_array($sku) && !empty($args['area'])) {
            $item = Item::whereIn('number', $sku)->get(['area', 'number AS sku',  'total AS num']);
            foreach ($item as $key => $value) {
                $item[$key]->desc = $value->num > 0 ? '有货' : '无货';
                $item[$key]->area = $args['area'];
            }
        }
        
        $data['success'] = true;
        $data['result'] = $item;
        echo json_encode($data, JSON_UNESCAPED_UNICODE);
        exit;
    }

    /*配件库接口－＞暂时未做*/
    public function getFittings()
    {
        $access_token = $_REQUEST['token'];
        parent::checkToken($access_token);
        $data['success'] = true;
        $data['result'] = '暂时未做';
        echo json_encode($data, JSON_UNESCAPED_UNICODE);
        exit;
    }

    /*查询商品延保服务接口*/
    public function getServices()
    {
        $access_token = $_REQUEST['token'];
        parent::checkToken($access_token);

        $skus = explode(',', $_REQUEST['skus']);

        $items = Item::whereIn('number', $skus)->get();
        $data['success'] = true;
        $result= [];
        $i = 0;
        foreach ($items as $key => $value) {
            if ($value->service_ids) {
                $result[$i]['sku_id'] = $value->number;
                $result[$i]['services']['name'] = $value->service_name;
                $result[$i]['services']['url'] = $value->service_url;
                $service_ids = explode(',', $value->service_ids);
                $result[$i]['services']['detail'] = Itemservice::whereIn('id', $service_ids)->get(['sku', 'name', 'title', 'price', 'position']);
                $i++;
            }
            
        }
        $data['result'] = $result;
        echo json_encode($data, JSON_UNESCAPED_UNICODE);
        exit;
    }
    
}