<?php
class Order extends Illuminate\Database\Eloquent\Model
{
	protected $table = 'shop2_zmaxshop_order';

	public $timestamps = false;
}
