<?php
class Invoice extends Illuminate\Database\Eloquent\Model
{
	protected $table = 'shop2_tax_einvoice';

	public $timestamps = false;
}
