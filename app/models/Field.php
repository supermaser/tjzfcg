<?php
class Field extends Illuminate\Database\Eloquent\Model
{
	protected $table = 'shop2_zmaxshop_field';

	public $timestamps = false;
}
